import React, { useState } from 'react';
import { getDirefenceYear, calcBrand, getPlan } from '../components/Helper';

const Form = ({form_class, form_g_class, saveResumen}) => {

    const [ datos, saveData ] = useState({
        brand: '',
        year: '',
        plan: ''
    });

    // guardar error
    const [ error, saveError ] = useState(false);


    // extraer valores state
    const { brand, year, plan } = datos;

    // leer datos form colocarlos state
    const getInfo = e => {
        saveData({
            ...datos,
            [e.target.name] : e.target.value
        })
    }

    // cuando se hace submit
    const cotizarSeguro = e => {
        e.preventDefault();

        if(brand.trim() === '' || year.trim() === '' || plan.trim() === '') {
            saveError(true);
            return;
        }

        saveError(false);

        // base 2000
        let result = 2000;

        // get diference between years
        // each year less3%
        const diference = getDirefenceYear(year);
        result -= (( diference * 3) * result) / 100;

        // american 15%
        // asian 5%
        // europen 30%
        result = calcBrand(brand) * result;

        // basic increase 20%
        // complete 50%
        const increasePlan = getPlan(plan);
        result = parseFloat(increasePlan * result).toFixed(2);
        
        console.log(result);

        saveResumen({
            cotizacion: result,
            datos
        })
    }
    

    return (
        <form 
            className={'form ' + form_class}
            onSubmit={cotizarSeguro}
        >

            { error ? <div className="alert alert-danger mb-3 text-center"> Todos los campos son obligatorios </div> : null }

            <div className={'form-group ' + form_g_class}>
                <label className="mb-0 mr-3">Brand</label>
                <select 
                    className="form-control"
                    name="brand"
                    value={brand}
                    onChange={getInfo}
                >
                    <option value="">---- Select ----</option>
                    <option value="american">American</option>
                    <option value="european">Euro</option>
                    <option value="asian">Asian</option>
                </select>
            </div>

            <div className={'form-group ' + form_g_class}>
                <label className="mb-0 mr-3">Year</label>
                <select 
                    className="form-control"
                    name="year"
                    value={year}
                    onChange={getInfo}
                >
                    <option value="">---- Select ----</option>
                    <option value="2022">2022</option>
                    <option value="2021">2021</option>
                    <option value="2020">2020</option>
                    <option value="2019">2019</option>
                    <option value="2018">2018</option>
                    <option value="2017">2017</option>
                    <option value="2016">2016</option>
                    <option value="2015">2015</option>
                    <option value="2014">2014</option>
                    <option value="2013">2013</option>
                    <option value="2012">2012</option>
                    <option value="2011">2011</option>
                    <option value="2010">2010</option>
                </select>
            </div>

            <div className={'form-group ' + form_g_class}>
                <label className="mb-0 mr-3">Plan</label>
                <input
                    type="radio"
                    name="plan"
                    value="basic"
                    checked={plan === "basic"}
                    onChange={getInfo}
                    className="mx-3"
                /> Basic

                <input
                    type="radio"
                    name="plan"
                    value="complete"
                    checked={plan === "complete"}
                    onChange={getInfo}
                    className="mx-3"
                /> Complete
            </div>

            <div className="form-group mb-0">
                <button type="submit" className="btn btn-primary btn-block">Enviar</button>
            </div>
        </form>
    );
}

export default Form;