import React from 'react';

const Header = ({title, clase}) => {
    return (
        <header className={'header ' + clase}>
            <div className="row py-3">
                <h1 className="col-12 text-center">{title}</h1>
            </div>
        </header>
    );
}

export default Header;


