import React from 'react';
import { mayus } from './Helper';

const Resumen = ({ datos }) => {

    // extraer de datos
    const { brand, year, plan } = datos;
    if( brand === '' || year === '' || plan === '' ) return null;

    return(
        <div className="col-12 col-md-7 mx-md-auto p-3 text-center mb-3">
            <h4>Resumen de Cotizacion</h4>
            <ul className="list-group col-9 col-md-6 mx-auto">
                <li className="list-group-item list-group-item-info">Brand: { mayus(brand) }</li>
                <li className="list-group-item list-group-item-info">Year: { mayus(year) }</li>
                <li className="list-group-item list-group-item-info">Plan: { mayus(plan) }</li>
            </ul>
        </div>
    );
}

export default Resumen;